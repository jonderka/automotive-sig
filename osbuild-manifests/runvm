#!/usr/bin/env python3

import argparse
import os
import platform
import shutil
import subprocess
import sys

is_verbosse = False
def print_verbose(s):
    if is_verbose:
        print(s)

def print_error(s):
    print(s, file=sys.stderr)

def exit_error(s):
    print_error(s)
    sys.exit(1)

def bool_arg(val):
    return "on" if val else "off"

def find_qemu(arch):
    if arch == platform.machine():
        binary_name = "qemu-kvm"
    else:
        binary_name = f"qemu-system-{arch}"

    for d in ["/usr/bin", "/usr/libexec"]:
        p = os.path.join(d, binary_name)
        if os.path.isfile(p):
            return p

    exit_error(f"Can't find {binary_name}")

def machine_id():
    with open("/etc/machine-id", "r") as f:
        mid = f.read().strip()
    return mid

def generate_mac_address():
    # create a new mac address based on our machine id
    data = machine_id()

    maclst = ["FE"] + [data[x:x+2] for x in range(-12, -2, 2)]
    return  ":".join(maclst)


def find_omvf():
    dirs = [
        "~/.local/share/ovmf",
        "/usr/share/OVMF"
    ]

    for d in dirs:
        path = os.path.expanduser(d)
        if os.path.exists(path):
            return path

    raise RuntimeError("Could not find OMVF")

def main():
    parser = argparse.ArgumentParser(description="Boot virtual machine images")
    parser.add_argument("--verbose", default=False, action="store_true")
    parser.add_argument("--arch", default=platform.machine(), action="store",
                        help=f"Arch to run for (default {platform.machine()})")
    parser.add_argument("--memory", default="2G",
                        help=f"Memory size (default 2G)")
    parser.add_argument("--nographics", default=False, action="store_true",
                        help=f"Run without graphics")
    parser.add_argument("--snapshot", default=False, action="store_true",
                        help=f"Work on a snapshot  of the image")
    parser.add_argument("--secureboot", dest="secureboot", action="store_true", default=False,
                        help="Enable SecureBoot")
    parser.add_argument("image", type=str, help="The image to boot")
    parser.add_argument('extra_args', nargs=argparse.REMAINDER, metavar="...", help="extra qemu arguments")

    args = parser.parse_args(sys.argv[1:])

    global is_verbose
    is_verbose = args.verbose

    native = platform.machine() == args.arch

    qemu = find_qemu(args.arch)
    qemu_args = [qemu]

    if args.arch == "x86_64":
        machine = "q35"
        default_cpu = "qemu64,+ssse3,+sse4.1,+sse4.2,+popcnt"

        omvf = find_omvf()
        if args.secureboot:
            qemu_args += [
                "-drive", f"file={omvf}/OVMF_CODE.secboot.fd,if=pflash,format=raw,unit=0,readonly=on",
                "-drive", f"file={omvf}/OVMF_VARS.secboot.fd,if=pflash,format=raw,unit=1,snapshot=on,readonly=off",
            ]
        else:
            qemu_args += [
                "-drive", f"file={omvf}/OVMF_CODE.fd,if=pflash,format=raw,unit=0,readonly=on",
                "-drive", f"file={omvf}/OVMF_VARS.fd,if=pflash,format=raw,unit=1,snapshot=on,readonly=off",
            ]
    elif args.arch == "aarch64":
        machine = "virt"
        default_cpu = "cortex-a57"
        qemu_args += [
            "-bios", "/usr/share/edk2/aarch64/QEMU_EFI.fd",
            "-boot", "efi"
        ]
    else:
        exit_error(f"unsupported architecture {args.arch}")

    use_kvm = native and os.path.exists("/dev/kvm")
    if use_kvm:
        qemu_args += ['-enable-kvm']
    else:
        print_verbose("Acceleration: off")

    qemu_args += [
        "-m", str(args.memory),
        "-machine", machine,
        "-cpu", "host" if use_kvm else default_cpu
    ]

    portfwd = {
        2222:22
    }
    for local, remote in portfwd.items():
        print_verbose(f"port: {local} → {remote}")

    fwds = [f"hostfwd=tcp::{h}-:{g}" for h, g in portfwd.items()]

    macstr = generate_mac_address()
    print_verbose(f"MAC: {macstr}")

    qemu_args += [
        "-device", f"virtio-net-pci,netdev=n0,mac={macstr}",
        "-netdev", "user,id=n0,net=10.0.2.0/24," + ",".join(fwds),
    ]

    if args.nographics:
        qemu_args += ["-nographic"]

    print_verbose(f"Image: {args.image}")

    qemu_args += [
        "-drive", f"file={args.image},index=0,media=disk,format=qcow2,if=virtio,snapshot={bool_arg(args.snapshot)}",
    ]

    qemu_args += args.extra_args

    print_verbose(f"Running: {' '.join(qemu_args)}")

    try:
        res = subprocess.run(qemu_args, check=False)
    except KeyboardInterrupt:
        exit_error("Aborted")

    return res.returncode


if __name__ == "__main__":
    sys.exit(main())
